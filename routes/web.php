<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('home');
});
Route::get('/daftar', function () {
    return view('daftar');
});
Route::get('/mahasiswa', function () {
    return view('mahasiswa');
});
Route::get('/programs', function () {
    return view('programs');
});
Route::get('/affiliated', function () {
    return view('affiliated');
});
Route::get('/kontak', function () {
    return view('kontak');
});